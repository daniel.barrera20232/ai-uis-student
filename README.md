# Inteligencia Artificial I 2023-1

## Bienvenidos!

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/raw/master/imgs/banner_IA.png"  width="1000px" height="200px">


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb) 

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local. 


## Calificación
- 30% Talleres
- 10% Talleres en clase. (participaciones por cuartiles)
- 30% Parciales
- 30% Proyecto funcional IA 

## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres ser desarrollará en casa, dentro de las fechas establecidas en el cronograma. 


## Parciales (Quizes)

Son evaluaciones **individuales** basadas en notebooks sobre los temas tratados en las clases. Los estudiantes deben solucionarlo en el salón de clase, en un tiempo determinado. Los apuntes y notebooks del curso se pueden utilizar (curso del repositorio). 


## Proyecto funcional IA

- **Funcionamiento del proyecto**: El proyecto se debe realizar como un notebook y debe ser 100% funcional.

- **Prototipo (PRE-SUS PROJ)**: En este item se considera como esta estructurado el proyecto y se espera una nivel razonable de funcionalidad.

- **Presentación**:
    - Imagen relacionada (800 x 300) con la siguiente información: título del proyecto e información de los estudiantes<br>
    - Video corto (Máximo 5 minutos) (ENTREGAR EL ARCHIVO DE VIDEO y también alojarlo en youtube)<br>
    - Archivo de las diapositivas

- **Sustentación**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 
Todos los items tienen el mismo porcentaje de evaluación. 

- **REPOSITORIO DEL PROYECTO**
    - Todos los archivos relacionados con el proyecto deben alojarse en un repositorio de los integrantes del estudainte
    - El archivo readme.md del repositorio debe tener la siguiente información:
        - Titulo del proyecto
        - Banner- Imagen de 800 x 300
        - Autores: Separados por comas
        - Objetivo: Una frase
        - Dataset: información con link de descarga
        - Modelos: Métodos usados para su desarrollo. Escribir solo palabras claves. 
        - Enlaces del código, video, y repositorio
    


**UNICAMENTE SE TENDRAN EN CUENTA LOS PROYECTOS QUE SE HAYAN POSTULADO AL FINALIZAR EL PRIMER CORTE**


## Calendario y plazos

                        SESSION 1            SESSION 2              SESSION SATURDAY


     W01 aug22-aug23    Intro                Python-general
     W02 aug29-aug30    Python-Numpy         Python-Numpy
     W03 sep05-sep06    Pandas               Pandas
     W04 sep12-sep13    Visualización        análisis de datos
     W05 sep19-sep20    PROYECTO-AVANCE 1    PROYECTO-AVANCE 1       Parcial 1
     W06 sep26-sep27    Intro & ML-Gauss     Clasificación A.M.S.
     W07 oct03-oct04    Clasificación A.M.S. Metodos A.M.S
     W08 oct10-oct11    Regresión A.M.S       Deep Learning
     W09 oct17-oct18    DNN-imgs-estruc      Deep Learning
     W10 oct24-oct25    DNN-audio            DNN-audio
     W11 oct31-nov01    PROYECTO-AVANCE 2    PROYECTO-AVANCE 2       parcial 2
     W12 nov07-nov08    No supervisado       No supervisado
     W13 nov14-nov15    Dim red: PCA         Dim red: tsn-e
     W14 nov21-nov22    Genetic Alg          Genetic Alg             Parcial 3  (2 a 4 pm)
     W15 nov28-nov29    PROYECTO-AVANCE 3    PROYECTO-AVANCE 3
     W16 dic05-dic06    SUS PROJ                SUS PROJ




     sept 29             -> Registro primera nota
     oct 13              -> Último día cancelación materias
     oct 13              -> Último dia cancelación de matricula académica
     dic 07              -> Finalización clase
     dic 11 - dic 13     ->  evaluaciones finales
     dic 13              -> Registro calificaciones finales
     dic 14              -> habilitaciones
     dic 15              -> Registro de calificaciones definitivas
     dic 18              -> Vacaciones de personal docente y administrativo

    


**ACUERDO n.° acuerdo 377, del 15 de noviembre de 2022**
[Calendario academico](https://uis.edu.co/wp-content/uploads/2022/12/Calendario-Academico-2023.pdf)


**CUALQUIER ENTREGA FUERA DE PLAZO SERÁ PENALIZADA CON UN 50%**

**LOS PROBLEMSETS ESTAN SUJETOS A CAMBIOS QUE SERÁN DEBIDAMENTE INFORMADOS**

**DEADLINE DE LOS PROBLEMSETS SERÁ EL DIA DE CADA PARCIAL**

